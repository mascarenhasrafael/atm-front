import axios from 'axios';
import { getToken, logout } from "./auth";



const api = axios.create({
  baseURL: 'https://atm-back.herokuapp.com'
});

api.interceptors.request.use(
  (config) => {
    const token = getToken();
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    alert(error);
  }
);
api.interceptors.response.use(
  (response)=> {
    return response;
  },
  (error)=>{
    if (error.response.status === 401) {
      logout();
      return Promise.reject(error);
    }
    return Promise.reject(error);
  }
)

export default api;