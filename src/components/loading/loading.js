import React from 'react'
import './loading.css'

const Loading = () => {
  return (
    <div className="loading">
      <img src="https://media1.tenor.com/images/d6cd5151c04765d1992edfde14483068/tenor.gif" alt="loading"/>
    </div>
  )
}

export default Loading
