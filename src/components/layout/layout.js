import React from 'react'
import Header from './../header/header'

const Layout = Props => {
  return (
    <div>
      <Header></Header>
      {Props.children}
    </div>
  )
}

export default Layout
