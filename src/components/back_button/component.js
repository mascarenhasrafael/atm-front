import React from 'react';
import { Link } from "react-router-dom";

import "./style.css"
import Image from "./back.png";

const BackButton = (props) => {
  return (
    <div>
      <Link to={props.to} className="Voltar">
        <img className="VoltarImg" src={Image} alt="botão para voltar à pagina anterior"></img>
      </Link>
    </div>
  )
}

export default BackButton
