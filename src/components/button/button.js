import React from "react";
import {Link} from "react-router-dom"
import './button.css'

function Button(Props){
  return(
    <Link className="Button" to={Props.path}>{Props.children}</Link>
  )
}

export default Button;