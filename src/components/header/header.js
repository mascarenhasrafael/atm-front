import React, { useContext } from 'react';
import { logout } from './../../services/auth';
import api from './../../services/api';
import './header.css';
import HeaderBtn from './header_button/button'

import { LoadingContext } from '../../routes'
import { BalanceContext } from '../../App'

const Header = () => {
  const loader = useContext(LoadingContext);
  const balance = useContext(BalanceContext);

  const accountFinish = () => {
    let answer = window.confirm("Tem certeza que deseja encerrar sua conta?");
    if (answer) {
      loader.setLoading(true);
      api.delete("/finishAccount").then(() => {
        logout();
        loader.setLoading(false);
      }
      )
        .catch(error => {
          alert(error);
          loader.setLoading(false);
        })
    }
  }

  return (
    <div className="Header">
      <HeaderBtn danger={true} handler={accountFinish}> Encerrar Conta </HeaderBtn>
      <div>
        <span>Saldo: {balance}</span>
        <HeaderBtn handler={logout}> Logout </HeaderBtn>
      </div>
    </div>
  )
}

export default Header
