import React from 'react'
import './button.css'

const HeaderBtn = Props => {
  let buttonClass = "Header-Btn"
  if (Props.danger) {
    buttonClass = "Header-Btn-Danger"
  }
  return (
    <span className={buttonClass} onClick={Props.handler}>{Props.children}</span>
  )
}

export default HeaderBtn
