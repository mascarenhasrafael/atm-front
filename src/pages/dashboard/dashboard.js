import React from 'react'
import Button from './../../components/button/button'

const Dashboard = () => {
  return (
    <div className="Container">
      <Button path="/app/saque">Saque</Button>
      <Button path="/app/extrato">Extrato</Button>
      <Button path="app/deposito">Depósito</Button>
      <Button path="/app/transferencia">Transferência</Button>  
    </div>
  )
}

export default Dashboard
