import React, { useState, useContext } from "react";
import { Link, useHistory } from "react-router-dom";

import api from '../../services/api';
import { login } from "../../services/auth";

import { LoadingContext } from '../../routes'

import '../../components/general_styles.css';
import './login.css';

function Login() {
  const [account, setAccount] = useState('');
  const [password, setPassword] = useState('');
  const loader = useContext(LoadingContext);
  let history = useHistory();

  function handleSubmit(event) {
    event.preventDefault();
    let credentials = {
      account: account,
      password: password
    }
    let token;
    if (credentials.account && credentials.password) {
      loader.setLoading(true);
      api.post('/login', credentials)
        .then(response => {
          token = response.data.token;
          login(token);
          history.push("/app");
          loader.setLoading(false);
        })
        .catch(() => {
          loader.setLoading(false);
          alert(`Conta e/ou senha não conferem`);
        })
    }
  }
  return (
    <div className="Container">
      <form onSubmit={handleSubmit}>
        <h1>Login</h1>

        <input
          className="Field"
          placeholder="Conta"
          type='text'
          id='account'
          name='account'
          value={account}
          onChange={({ target }) => setAccount(target.value)}
          required={true}
        />

        <input
          className="Field"
          placeholder="Senha"
          type='password'
          id='password'
          name='password'
          value={password}
          onChange={({ target }) => setPassword(target.value)}
          required={true}
        />

        <span>
          Não tem conta? <Link to="/cadastro">Solicite já</Link>
        </span>

        <input
          className="submitButton"
          type='submit'
          value='Entrar'
        />
      </form>
    </div>
  )
}

export default Login;