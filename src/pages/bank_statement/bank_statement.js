import React, { useState, useEffect, useContext } from 'react'
import api from '../../services/api'
import './bank_statement.css'
import { LoadingContext } from '../../routes'
import BackButton from '../../components/back_button/component'

const BankStatement = () => {
  const [statement, setStatement] = useState([])
  let transactions = [];
  const loader = useContext(LoadingContext);

  const getStatement = () => {
    loader.setLoading(true);
    api.get('/transactions')
      .then((response) => {
        setStatement(response.data);
        loader.setLoading(false);
      }
      )
  }
  useEffect(getStatement, []);

  transactions = statement.map((item) => {
    let tipo;
    if (item.kind === 'withdraw') {
      tipo = 'saque';
    }
    else if (item.kind === 'deposit') {
      tipo = 'depósito';
    }
    else if (item.kind === 'transfer') {
      tipo = 'transferência';
    }
    return (
      <div key={item.id} className="Transacao">
        <div className="Descricao">{item.description}</div>
        <div className="Tipo">{tipo}</div>
        <div className="Valor">{item.value}</div>
      </div>
    );
  })

  return (
    <div>
      <BackButton to="/app"></BackButton>
      <div className="Extrato">
        <div className="Cabecalho">
          <div className="Descricao">Descrição</div>
          <div className="Tipo">Tipo</div>
          <div className="Valor">Valor</div>
        </div>
        {transactions}
      </div>
    </div>
  )
}

export default BankStatement
