import React, { useState, useContext } from 'react'
import { useHistory } from "react-router-dom";
import api from '../../services/api';

import BackButton from "../../components/back_button/component"

import { LoadingContext } from '../../routes';
import { BalanceContext } from '../../App';

const Withdraw = () => {
  const loader = useContext(LoadingContext)
  const [value, setValue] = useState("");
  const [password, setPassword] = useState("");
  const balance = useContext(BalanceContext);

  let history = useHistory();

  function handleSubmit(event) {
    event.preventDefault();
    let transaction = {
      value: value,
      kind: "withdraw",
      password: password
    }

    if ((value > 0) && (value < balance)) {
      loader.setLoading(true);
      api.post('/withdraw', transaction)
        .then(() => {
          history.push("/app");
          loader.setLoading(false);
          alert("Saque realizado!");
        })
        .catch((error) => {
          loader.setLoading(false);
          alert(error.message);
        })
    }
    else {
      alert("Saque precisa ter valor válido");
    }
  }

  return (
    <div>
      <BackButton to="/app"></BackButton>
      <div className="Container">
        <form onSubmit={handleSubmit}>
          <h1>Saque</h1>

          <input
            className="Field"
            placeholder="Valor"
            type='number'
            id='value'
            name='value'
            value={value}
            onChange={({ target }) => setValue(target.value)}
            required={true}
          />

          <input
            className="Field"
            placeholder="Confirme sua senha"
            type='password'
            id='password'
            name='password'
            value={password}
            onChange={({ target }) => setPassword(target.value)}
            required={true}
          />

          <input
            className="submitButton"
            type='submit'
            value='Sacar'
          />
        </form>
      </div>
    </div>
  )
}

export default Withdraw
