import React from "react";
import Button from "../../components/button/button"
import '../../components/general_styles.css'

function Home(){
  return(
    <div className="Container">
      <Button path="/login">Login</Button>
      <Button path="/deposito">Depósito</Button>
    </div>
  )
}

export default Home;