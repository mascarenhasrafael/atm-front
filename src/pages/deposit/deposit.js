import React, { useState, useContext } from 'react'
import { useHistory } from "react-router-dom";
import api from '../../services/api';

import { isAuthenticated } from '../../services/auth';
import { LoadingContext } from '../../routes';
import BackButton from '../../components/back_button/component';

const Deposit = () => {
  const loader = useContext(LoadingContext)
  const [destinyAccount, setDestinyAccount] = useState(null);
  const [value, setValue] = useState(null);
  const [description, setDescription] = useState('');
  const returnRoute = ((isAuthenticated()) ? '/app' : '/');

  let history = useHistory();

  function handleSubmit(event) {
    event.preventDefault();
    let transaction = {
      destination_id: destinyAccount,
      value: value,
      description: description,
      kind: "deposit"
    }
    if (destinyAccount > 0) {

      if (value > 0) {
        loader.setLoading(true);
        api.post('/deposit', transaction)
          .then(() => {
            history.push(returnRoute);
            loader.setLoading(false);
            alert("Deposito realizado!");
          })
          .catch((error) => {
            loader.setLoading(false);
            alert(error.message);
          })
      }
      else {
        alert("Depósito precisa ter valor válido");
      }
    } else {
      alert("conta invalida");
    }
  }
  return (
    <div>
      <BackButton to={returnRoute}></BackButton>
      <div className="Container">
        <form onSubmit={handleSubmit}>
          <h1>Depósito</h1>

          <input
            className="Field"
            placeholder="Conta de destino"
            type='number'
            id='destinyAccount'
            name='destinyAccount'
            value={destinyAccount}
            onChange={({ target }) => setDestinyAccount(target.value)}
            required={true}
          />

          <input
            className="Field"
            placeholder="Valor"
            type='number'
            id='value'
            name='value'
            value={value}
            onChange={({ target }) => setValue(target.value)}
            required={true}
          />

          <input
            className="Field"
            placeholder="Descrição"
            type='textarea'
            id='description'
            name='description'
            value={description}
            onChange={({ target }) => setDescription(target.value)}
          />

          <input
            className="submitButton"
            type='submit'
            value='Depositar'
          />
        </form>
      </div>
    </div>
  )
}


export default Deposit
