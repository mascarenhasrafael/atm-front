import React, { useState, useContext } from 'react';
import { useHistory } from "react-router-dom";
import api from '../../services/api';

import { LoadingContext } from '../../routes';

const SignUp = () => {
  const [name, setName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [emailConfirmation, setEmailConfirmation] = useState('');

  const loader = useContext(LoadingContext);
  let history = useHistory();

  function handleSubmit(event) {
    event.preventDefault();
    let newAccount = {
      name: name,
      lastName: lastName,
      email: email
    }
    if (email === emailConfirmation) {
      loader.setLoading(true);
      api.post('/accounts', newAccount)
        .then(() => {
          history.push("/login");
          loader.setLoading(false);
          alert("Senha e numero de conta enviados para o seu e-mail");
        })
        .catch((error) => {
          loader.setLoading(false);
          if (error.response.status === 422) {
            alert("Email já em uso por outro usuário");
          }
        })
    }
    else {
      alert("email e confirmação de email não são iguais");
    }
  }
  return (
    <div className="Container">
      <form onSubmit={handleSubmit}>
        <h1>Cadastro</h1>

        <input
          className="Field"
          placeholder="Nome"
          type='text'
          id='name'
          name='name'
          value={name}
          onChange={({ target }) => setName(target.value)}
          required={true}
        />

        <input
          className="Field"
          placeholder="Sobrenome"
          type='text'
          id='lastName'
          name='lastName'
          value={lastName}
          onChange={({ target }) => setLastName(target.value)}
          required={true}
        />

        <input
          className="Field"
          placeholder="E-mail"
          type='email'
          id='email'
          name='email'
          value={email}
          onChange={({ target }) => setEmail(target.value)}
          required={true}
        />

        <input
          className="Field"
          placeholder="Confirmação do e-mail"
          type='email'
          id='email-confirmation'
          name='email-confirmation'
          value={emailConfirmation}
          onChange={({ target }) => setEmailConfirmation(target.value)}
          required={true}
        />
        <input
          className="submitButton"
          type='submit'
          value='Enviar'
        />
      </form>
    </div>
  )
}


export default SignUp
