import React, { useState, useContext } from 'react';
import { useHistory} from "react-router-dom";
import api from '../../services/api';

import BackButton from "../../components/back_button/component"

import { LoadingContext } from '../../routes';
import { BalanceContext } from '../../App';

const Transfer = () => {
  const loader = useContext(LoadingContext)
  const [value, setValue] = useState("");
  const [password, setPassword] = useState("");
  const [description, setDescription] = useState("");
  const [destination, setDestination] = useState("");
  const balance = useContext(BalanceContext);

  let history = useHistory();

  function handleSubmit(event) {
    event.preventDefault();
    let transaction = {
      value: value,
      kind: "transfer",
      description: description,
      destination_id: destination,
      password: password
    }

    if ((value > 0) && (value < balance)) {
      loader.setLoading(true);
      api.post('/transfer', transaction)
        .then(() => {
          history.push("/app");
          loader.setLoading(false);
          alert("Transferência realizada!");
        })
        .catch((error) => {
          loader.setLoading(false);
          const errorCode = error.response.status;
          if (errorCode === 404) {
            alert("Conta de destino inválida, verifique as informações nos campos");
          } else if (errorCode === 422) {
            alert("Impossível realizar a Operação");
          }
        })
    }
    else {
      alert("Transferência precisa ter valor válido");
    }
  }

  return (
    <div>
      <BackButton to="/app"></BackButton>
      <div className="Container">
        <form onSubmit={handleSubmit}>
          <h1>Transferência</h1>

          <input
            className="Field"
            placeholder="Conta de destino"
            type='number'
            id='destination'
            name='destination'
            value={destination}
            onChange={({ target }) => setDestination(target.value)}
            required={true}
          />

          <input
            className="Field"
            placeholder="Valor"
            type='number'
            id='value'
            name='value'
            value={value}
            onChange={({ target }) => setValue(target.value)}
            required={true}
          />

          <input
            className="Field"
            placeholder="Descrição"
            type='textarea'
            id='description'
            name='description'
            value={description}
            onChange={({ target }) => setDescription(target.value)}
            required={true}
          />

          <input
            className="Field"
            placeholder="Confirme sua senha"
            type='password'
            id='password'
            name='password'
            value={password}
            onChange={({ target }) => setPassword(target.value)}
            required={true}
          />

          <input
            className="submitButton"
            type='submit'
            value='Transferir'
          />
        </form>
      </div>
    </div>
  )
}

export default Transfer
