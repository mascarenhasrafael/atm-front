import React, { createContext, useState } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import { isAuthenticated } from "./services/auth";
import Loader from './components/loading/loading';

import Home from './pages/home/home';
import SignUp from './pages/signup/signup';
import Deposit from './pages/deposit/deposit';
import Login from './pages/login/login';
import App from './App';
import PageNotFound from './PageNotFound';

export const LoadingContext = createContext();

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={
      props => isAuthenticated() ? (
        <Component {...props} />
      ) : (
          <Redirect to={{ pathname: "/login", state: { from: props.location } }} />
        )
    }
  />
);

const Routes = () => {
  const [isLoading, setLoading] = useState(false)
  const loader = {
    isLoading,
    setLoading
  }
  return (
    <LoadingContext.Provider value={loader}>
    {isLoading && <Loader />}
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/deposito" component={Deposit} />
          <Route exact path="/cadastro" component={SignUp} />
          <Route path="/login" component={Login} />
          <PrivateRoute path="/app" component={App} />
          <Route path="*" component={PageNotFound} />
        </Switch>
      </BrowserRouter>
    </LoadingContext.Provider>
  )
};

export default Routes;