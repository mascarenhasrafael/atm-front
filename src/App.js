import React, { createContext, useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Layout from './components/layout/layout'

import Dashboard from './pages/dashboard/dashboard'
import BankStatement from './pages/bank_statement/bank_statement'
import Deposit from './pages/deposit/deposit'
import Withdraw from './pages/withdraw/withdraw'
import Transfer from './pages/transfer/transfer'
import PageNotFound from './PageNotFound'
import api from './services/api';

export const BalanceContext = createContext();

const appBaseRoute = "/app";

const appRoute = route => {
  return (appBaseRoute + route);
}

const App = () => {
  const [balance, setBalance] = useState(null);
  api.get('/myAccount').then((response) => {
    setBalance(response.data.balance);
  })
  return (
    <BalanceContext.Provider value={balance}>
      <Layout>
        <Router>
          <Switch>
            <Route exact path={appRoute("")} component={Dashboard} />
            <Route exact path={appRoute("/extrato")} component={BankStatement} />
            <Route exact path={appRoute("/saque")} component={Withdraw} />
            <Route exact path={appRoute("/transferencia")} component={Transfer} />
            <Route exact path={appRoute("/deposito")} component={Deposit} />
            <Route path={appRoute("/*")} component={PageNotFound} />
          </Switch>
        </Router>
      </Layout>
    </BalanceContext.Provider>
  );
}

export default App;
